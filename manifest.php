<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['name']        = __( 'Text Extension', 'fw' );
$manifest['description'] = __( 'Description test ext', 'fw' );
$manifest['bitbucket'] = 'eremia_viorel/test';
$manifest['author'] = 'viorel';
$manifest['version'] = '1.2.3';
$manifest['display'] = true;
$manifest['standalone'] = true;
